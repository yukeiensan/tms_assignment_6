# Installing requirements
`pip3 install selenium argparse plotly`

# Usage
```
usage: inflation_simulator.exe [-h] [--file FILE] [--oneshot]
                               [--output OUTPUT] [--force]

Give statistics for a precise asked good

optional arguments:
  -h, --help       show this help message and exit
  --file FILE      Input file containing data (It will improve program setup
                   speed)
  --oneshot        Will run the program without relying on data file (using
                   --force will force output save - even if the program
                   doesn't need it to run)
  --output OUTPUT  Output filename to improve rendering speed (Rendered as
                   data.csv unless you use --oneshot or along with --force)
  --force          Force the output saving (Will be ignored if used without
                   --oneshot)
```
