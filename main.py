#!/usr/bin/env python3
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from selenium import webdriver
import progressbar
import argparse
import csv
import os
import re


# CLI functions
def terminate(data):
    data["running"] = False

    return data


def show_credits(data):
    print("Author:\t\tAntoine 'Yukeien' Duez")
    print("Email:\t\tyukeiensan@gmail.com")
    print("Version:\t1.0")
    print("\nThank you for using my software, hope you enjoy it.")

    return data


def show_command_list(data):
    print("\nList of commands:")
    for command in COMMAND_LIST:
        print(command)
    print()

    return data


def print_detail_list(keys):
    print("Available choices:")
    for index, elem in enumerate(keys):
        print("[" + str(index + 1) + "] - " + elem)


def get_product_index(data):
    choice_index = 0
    if not isinstance(data, int):
        keys = list(data.keys())
        print_detail_list(keys)
        number = None
    else:
        number = data

    while not isinstance(number, int):
        if 1 <= choice_index <= len(keys):
            if isinstance(data, int):
                number = data
            else:
                number = get_product_index(data[keys[choice_index - 1]])
        else:
            try:
                choice_index = int(input("$>"))
            except ValueError:
                print("Error: Invalid number")
            if not 1 <= choice_index <= len(keys):
                print("Please enter a number between 1 and " + str(len(keys)))

    return number


def get_product_data(data):
    print(data["products"]["Flour"].keys())
    data["index_asked"] = get_product_index(data["products"])

    return data


# Global Variables
DRIVER_BIN = os.path.join(os.getcwd(), './chromedriver')
BASE_URL = "https://data.bls.gov/timeseries/"
MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November",
          "December"]
COLORS = ['rgb(31, 119, 180)', 'rgb(255, 127, 14)',
          'rgb(44, 160, 44)', 'rgb(214, 39, 40)',
          'rgb(148, 103, 189)', 'rgb(140, 86, 75)',
          'rgb(227, 119, 194)', 'rgb(127, 127, 127)',
          'rgb(188, 189, 34)', 'rgb(23, 190, 207)', 'rgb(75, 86, 125)']
COMMAND_LIST = {
    "product": get_product_data,
    "exit": terminate,
    "credits": show_credits,
    "help": show_command_list
}


# Scraper functions
def parsing_arguments():
    parser = argparse.ArgumentParser(description="Give statistics for a precise asked good")
    parser.add_argument("--file", default="save.csv",
                        help="Input file containing data (It will improve program setup speed)")
    parser.add_argument("--oneshot", action="store_true", help="Will run the program without relying on data file "
                                                               "(using --force will force output save"
                                                               " - even if the program doesn't need it to run)")
    parser.add_argument("--output", default="save.csv", help="Output filename to improve rendering speed "
                                         "(Rendered as data.csv unless you use --oneshot or along with --force)")
    parser.add_argument("--force", action="store_true", help="Force the output saving "
                                                             "(Will be ignored if used without --oneshot)")
    args = parser.parse_args()

    return args


def clear():
    if os.name == 'nt':
        _ = os.system('cls')
    else:
        _ = os.system('clear')


def create_chrome_session(driver, url):
    driver.get(url)

    return driver


def inside(check_list, key, s):
    for cell in check_list:
        if cell[key] == s:
            return True

    return False


def gather_product_info(driver, raw_data):
    list_size = len(raw_data)

    for index, cell in enumerate(raw_data):
        driver = create_chrome_session(driver, BASE_URL + cell["id"])
        core = driver.find_element_by_id("bodytext")
        cell[cell["id"]] = str(core.find_element_by_xpath(".//table[2]/caption/pre").text).split("\n")[1]
        cell[cell["id"]] = re.sub(r" +", " ", cell[cell["id"]]).split("Series Title: ")[1]
        cell["info"] = []

        row_list = core.find_elements_by_xpath(".//table[2]/tbody/tr")
        print("Retrieving " + cell[cell["id"]] + " data:")
        with progressbar.ProgressBar(max_value=12 * len(row_list), widgets=[
            progressbar.Timer(),
            progressbar.Bar(),
            " [",
            progressbar.Percentage(),
            "] (",
            progressbar.Counter(format="%(value)02d/%(max_value)d"),
            ")"
        ]) as progress_info:
            for row_index, row in enumerate(row_list):
                obj = {
                    "year": row.find_element_by_xpath(".//th").text
                }
                info_list = row.find_elements_by_xpath(".//td")
                tab = []
                for info_index, info in enumerate(info_list):
                    cell_info = re.sub("[^0-9.]", "", info.text)
                    if cell_info != "":
                        tab.append(float(cell_info))
                    else:
                        tab.append(None)
                    progress_info.update(12 * row_index + info_index)
                obj["data"] = tab
                cell["info"].append(obj)
        print("Total: [" + str(round((index + 1) * 100 / list_size, 2)) + '%]' +
              " (" + str(index + 1) + '/' + str(list_size) +
              ") ~ Please wait while we download required data.\n")

    return raw_data


def build_object(raw_data):
    data = []

    for index, row in enumerate(raw_data):
        if index > 0 and not inside(data, "id", row[0]):
            data.append({
                "id": row[0]
            })

    return data


def get_raw_data(driver):
    driver = create_chrome_session(driver, "https://download.bls.gov/pub/time.series/ap/ap.data.0.Current")
    raw_data = driver.find_element_by_xpath(".//html/body/pre").text

    raw_data = re.sub(r" +", ",", raw_data)

    data = list(csv.reader(raw_data.split("\n")))
    data = build_object(data)
    data = gather_product_info(driver, data)

    return data


def build_from_save(content):
    data = [{
        "id": "first",
        "info": []
    }]
    counter = -1
    first = True

    content.pop(0)
    for index, row in enumerate(content):
        if not first and data[counter]["id"] == row[0]:
            info = data[counter]["info"]
            info.append({
                "year": row[2],
                "data": row[3].split("|")
            })
        else:
            data.append({})
            first = False
            counter += 1

            data[counter]["id"] = row[0]
            data[counter][row[0]] = row[1]
            data[counter]["info"] = []
            data[counter]["info"].append({
                    "year": row[2],
                    "data": row[3].split("|")
                })

    data.pop(len(data) - 1)
    return data


def read_save(filename):
    file = open(filename, "r+")

    file_content = list(csv.reader(file))
    data = build_from_save(file_content)

    return data


def keep_data(filename, data_raw):
    file = open(filename, "w+")

    file.write("id,name,year,data\n")
    for product in data_raw:
        for row in product["info"]:
            data = ""
            for index, cell in enumerate(row["data"]):
                if len(row["data"]) - 1 != index:
                    data += str(cell) + "|"
                else:
                    data += str(cell)
            file.write(product["id"] + ',"' + product[product["id"]] + '",' + row["year"] + ',' + data + '\n')


def calculate_inflation(info):
    rows_cpi = []
    rows_inflation = []

    for data in info:
        row = []
        for cell in data["data"]:
            if cell == "None" or info[0]["data"][0] == "None":
                row.append(0.0)
            else:
                row.append(float(cell)/float(info[0]["data"][0])*100)
        rows_cpi.append(row)

    last_x = -1
    last_y = 0
    for index, cpi in enumerate(rows_cpi):
        row = []
        for x_index, cell in enumerate(cpi):
            if last_x == -1:
                row.append(0)
                last_x = 0
            else:
                row.append(round((cell - rows_cpi[last_y][last_x]) / rows_cpi[last_y][last_x] * 100, 2))
                last_x = x_index
        last_y = index
        rows_inflation.append(row)

    return rows_inflation


def display_graph(data):
    print("Rendering charts of: " + data[data["id"]])
    inflation_list = calculate_inflation(data["info"])
    fig = make_subplots(rows=2, cols=1, subplot_titles=("Price variation", "Inflation rate"))

    for index, info in enumerate(data["info"]):
        fig.add_trace(go.Scatter(x=MONTHS, y=info["data"], name=info["year"], line=dict(color=COLORS[index], width=2)),
                      row=2, col=1)
        fig.add_trace(go.Scatter(x=MONTHS, y=inflation_list[index],
                                 line=dict(color=COLORS[index], width=2)), row=2, col=1)
    fig.update_layout(title="Average Price of " + data[data["id"]],
                      xaxis_title="Month",
                      yaxis_title="Price (USD)")
    fig.show()


def command_line(data):
    data["running"] = True

    print("Welcome in product chart simulator type `help` for command list")
    print("Be aware that price can be blank for some time periods, "
          "it's due to lack of data, do not take these into account")
    print("Inflation rate is displayed on hover of any dot and is the inflation since the last month")
    while data["running"]:
        command = input("$> ")
        if command in COMMAND_LIST.keys():
            data = COMMAND_LIST[command](data)
        else:
            print("Error: unrecognized command, type `help` for command list")

        if data["index_asked"] is not None:
            display_graph(data["data"][data["index_asked"]])
            data["index_asked"] = None


def confirm(data, args):
    if args.output:
        data["output"] = args.output
    else:
        data["output"] = "data.csv"

    if args.oneshot and not args.force:
        print("Oneshot argument will require the program to gather all the needed data yet we won't store anything.")
    elif args.oneshot and args.force:
        print("Oneshot argument will require the program to gather all the needed data but don't worry "
              "we'll store everything to " + data.output)
    else:
        print("It seems you don't have the provided data.csv or you removed it. "
              "We'll have to build everything from scratch.")
    print("It will take a while you should get you some coffee.")
    print("Proceed anyway? [Y]es/[n]o (Answering 'no' will exit the program):")
    command = input("$> ")

    while command != "Y" or command != "n":
        if command == "Y":
            return True
        elif command == "n":
            return False
        else:
            print("Please answer [Y] for yes or [n] for no.")
        command = input("$> ")


def fill_products(obj, name, index):
    if name != "":
        name = name.split(", ")
        tmp_name = name[0]
        name.pop(0)
        name = ", ".join(name)
        if not tmp_name in obj:
            obj[tmp_name] = {}
        obj[tmp_name] = fill_products(obj[tmp_name], name, index)

        return obj
    else:
        obj = index

        return obj


def create_product_list(data):
    products = {}

    for index, product in enumerate(data):
        product_name = product[product["id"]]
        products = fill_products(products, product_name, index)

    return products


def main():
    args = parsing_arguments()
    data = {
        "input": args.file,
        "data": [],
        "index_asked": None
    }

    if os.path.exists(data["input"]) and not args.oneshot:
        data["data"] = read_save(data["input"])
        data["products"] = create_product_list(data["data"])
        command_line(data)
    elif args.oneshot and confirm(data, args):
        driver = webdriver.Chrome(executable_path=DRIVER_BIN)
        driver.implicitly_wait(30)
        data["data"] = get_raw_data(driver)
        data["products"] = create_product_list(data["data"])
        command_line(data)
    elif not os.path.exists(data["input"]) and confirm(data, args):
        driver = webdriver.Chrome(executable_path=DRIVER_BIN)
        driver.implicitly_wait(30)
        data["data"] = get_raw_data(driver)
        data["products"] = create_product_list(data["data"])
        command_line(data)
    else:
        print("Input file not found, please check the file you passed as argument exists")

    if args.force:
        keep_data(args.output, data["data"])
    elif not args.oneshot:
        keep_data(args.output, data["data"])

    print("$> Goodbye!")


if __name__ == "__main__":
    main()
